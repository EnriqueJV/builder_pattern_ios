import Foundation



enum CarType {
    case
    sportage,
    saloon,
    mulsanne
}

enum GearType {
    case
    manual,
    automatic
}

class Car: CustomStringConvertible {
    var color: String
    var numberOfSeats: Int
    var numberOfWheels: Int
    var type: CarType
    var gearType: GearType

    
    var description: String {
        return "color: \(color)\nNumber of seats: \(numberOfSeats)\nNumber of Wheels: \(numberOfWheels)\n Type: \(gearType)"
    }
    
    init(color: String, numberOfSeats: Int, numberOfWheels: Int, type: CarType, gearType: GearType) {
        
        self.color = color
        self.numberOfSeats = numberOfSeats
        self.numberOfWheels = numberOfWheels
        self.type = type
        self.gearType = gearType        
    }
}

class carBuild{
    var color = "Negro"
    var numberOfSeats  = 5
    var numberOfWheels = 4
    var type: CarType = .saloon
    var gearType: GearType = .automatic
    func buildCar() -> Car {
        return Car(color: color, numberOfSeats: numberOfSeats, numberOfWheels: numberOfWheels, type: type, gearType: gearType)
    }
}

var builder = carBuild()
let car1 = builder.buildCar()
print(car1.description)

builder.color = "Azul"
let car2 = builder.buildCar()
print(car2.description)
